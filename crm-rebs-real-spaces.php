<?php
/**
 * Plugin Name: CRM REBS Real Space Importer
 * Plugin URI: http://crm.rebs-group.com
 * Description: Plugin that handles importing properties from CRM REBS to Real Space WP theme
 * Version: 0.1
 * Author: Felix Kerekes
 * Author URI: http://crm.rebs-group.com
 * License:
 */

class CrmRebsRealSpacesImporter {

  /**
  * Construct the plugin object
  */
  public function __construct()
  {
    add_action( 'crm_rebs_check_all_properties', array('CrmRebsRealSpacesImporter', 'check_all_properties') );
  // register actions
  } // END public function __construct

  /**
   * Activate the plugin
   */
  public static function activate()
  {
    wp_schedule_event( time(), 'hourly', 'crm_rebs_check_all_properties' );
  }

  /**
   * Deactivate the plugin
   */
  public static function deactivate()
  {
    wp_clear_scheduled_hook('crm_rebs_check_all_properties');
  }


  public static function get_property_data($property) {
    $post = array(
      'post_type' => 'property',
      'post_status'   => 'publish',
      'post_title' => $property['title'],
      'post_content' => $property['description']
    );

    return $post;
  }

  /**
   * Set custom data (taxonomies) for a post, suitable for Real Spaces plugin.
   */
  public static function set_property_custom_data($post_id, $property) {
    // Transaction type
    wp_delete_object_term_relationships($post_id, 'property-contract-type');
    if ($property['for_sale']) {
      wp_set_object_terms($post_id, 'Vânzare', 'property-contract-type', true);
    }
    if ($property['for_rent']) {
      wp_set_object_terms($post_id, 'Închiriere', 'property-contract-type', true);
    }

    // Property type
    $value_arrays = CrmRebsImporter::get_value_arrays();
    $property_types = $value_arrays['property_type'];
    wp_set_object_terms($post_id, $property_types[$property['property_type']], 'property-type');
  }

  public static function update_property($property) {
    error_log("Updating property CP" . $property['id']);
    // Check if a property with this ID already exists
    $args = array(
      'post_type' => 'property',
      'meta_query' => array( array(
          'key' => 'crm_rebs_id',
          'value' => $property['id']
        )
      )
    );
    $properties = get_posts($args);

    // Get the WP post data
    $post = CrmRebsRealSpacesImporter::get_property_data($property);

    // Edit the property if it already exists
    if ($properties) {
      $post['ID'] = $properties[0]->ID;
    }

    // Add/update the property
    $post_id = wp_insert_post($post);

    if ($post_id) {
      // Populate the property with data
      CrmRebsRealSpacesImporter::set_property_custom_data($post_id, $property);

      $meta_id = add_post_meta($post_id, 'crm_rebs_id', (string)$property['id']);
    }

    $property['wp_id'] = $post_id;

    return $property;
  }

  /**
   * Check all properties and delete those that are not in the API anymore.
   */
  public static function check_all_properties() {
    // Get a list of all `property` type posts
    $args = array(
      'post_type' => 'property'
    );
    $properties = get_posts($args);

    // Delegate the actual check to the CRM REBS Importer plugin
    foreach ($properties as $property) {
      do_action( 'crm_rebs_check_property', $property->ID);
    }
  }

}

if (class_exists('CrmRebsRealSpacesImporter')) {
  // Installation and uninstallation hooks
  register_activation_hook(__FILE__, array('CrmRebsRealSpacesImporter', 'activate'));
  register_deactivation_hook(__FILE__, array('CrmRebsRealSpacesImporter', 'deactivate'));
  add_filter( 'crm_rebs_update_property', array('CrmRebsRealSpacesImporter', 'update_property'));
}

?>
